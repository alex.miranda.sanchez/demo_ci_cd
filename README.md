# py_config-map

_Configmap en un proyecto de Python_

### Instalación 🔧

_Crear un proyecto Python and Flask_

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    app.config.from_pyfile('/config/constants.cfg')
    return app.config['MSG_NAME_USER']


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=80)
```

_Crear el file utils/constans.cfg_

```
MSG_NAME_USER = "Alex dede El jardin de mi casa"
MSG_APODO_USER = "Perros"
```

_Crear el Dockerfile_

```
FROM tiangolo/uwsgi-nginx-flask:python3.7

COPY helloworld.py .
COPY utils/constants.cfg /config/constants.cfg

#RUN python helloworld.py
```

_Construir y correr la imagen Docker_

```
docker build -t apppython .

docker run -p 8080:80 -d apppython

```

_Comprobar que responda la app python_

```
curl localhost:8080
  Hello World from Flask
```

_Crear el file configmap.yaml_

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: appconfig
data:
  config.cfg: |
    MSG="Welcome to Flask!"
```

_Crear el yml para los valores del pod en Kubernetes_

```
kind: Pod
apiVersion: v1
metadata:
  name: apppython
spec:
  containers:
    - name: apppython
      image: apppython
      volumeMounts:
      - name: config-vol
        mountPath: /config
  volumes:
  - name: config-vol
    configMap:
      name: appconfig
```

_Aplicar el configmap_

```
kubectl apply -f configmap.yml

kubectl apply -f pod.yml
```

_Ejecutar el pod y probar_

```
kubectl exec -it app -- bash

	root@app:/app# curl localhost
		Welcome to Flask
```

_Modificar el file configmap.ym para ver los nuevos cambios_

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: appconfig
data:
  config.cfg: |
    MSG="Welcome to Kubernetes!"

```

_Aplicar y probar_

```
kubectl apply -f configmap.yml

kubectl exec -it app -- bash

	root@app:/app# curl localhost
		Welcome to Kubernetes!
```
## Autor ✒️

* **Alejandro Miranda** - *Initial Commit* - [alex.miranda](https://gitlab.com/alex.miranda.sanchez/)